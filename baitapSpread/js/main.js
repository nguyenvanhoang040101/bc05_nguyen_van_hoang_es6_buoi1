const heading = document.querySelector(".heading");
(jump = (a) => {
  return [...a].map((a) => `<span>${a}</span>`).join("");
}),
  (heading.innerHTML = jump(heading.innerText));
/**
 * Sử dụng arrow function và xây dựng hàm thêm hiệu ứng jump text
 Lấy nội dung text “Hover Me!” của class heading
 Sử dụng Spread Operator để tách từng ký tự của text “Hover Me!” =>
[“H”,”o”,”v”,”e”,”r” ,“M”,”e”,”!”]
 Tạo các thẻ span chứa các ký tự chữ sau khi đã tách được và thêm các thẻ span đó vào
thẻ heading
 Code mẫu sử dụng Spread Operator để tách từng ký tự

 */
